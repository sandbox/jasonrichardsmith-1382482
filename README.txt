Information
-----------
This module was developed from the IMIS Auth Module, which uses a different
SSO then IBC SSO and returns different data.

This module works with IBC SSO using SOAP.  You will need to purchase the
IMIS module from IBC for this to work.  The developer of this module is in no
way affiliated with IBC outside of being a customer.

This module works differently than the IMIS Authentication module.

This module loads the following information from the SSO.
IMIS ID
Member Type
First Name
Last Name
Prefix
Suffix
Full Name
Company Name
Workphone

The IMIS admin can also attach "Extension Data" which can be any sort of user
information query.  This information is loaded into a long text field since we
never know what it will return.

The SSO also returns information to create a cookie.  If users are in the same
domain they can access the IMIS web interface using this cookie.  Then the
cookie is destroyed on logout.

From original Auth module.
--------------------------
Exempt Users

Certain Drupal users can be exempted from iMIS Authentication (e.g. Drupal site
admins). This permits their login, password and other behaviours to be handled
normally by Drupal. These exempt users can be assigned to additional roles (e.g.
content editors) as needed by administrators.

Why a seperate module
---------------------
The IMIS Auth module uses a different web interface (REST) and is maintained by
the people people that sell the IMIS module.  I don't see them as having any
interest in maintaining or integrating this service because it is a competing
project.

Links
-----
IBC website:
http://www.ibconcepts.com/public/Home3/AM/ContentManagerNet/HomePages/
IBC_Live_HomePage.aspx?Section=Home3
Original IMIS Auth module
http://drupal.org/project/imis_auth
